import javax.swing.*;

public class prueba {

    public static void main(String[] args){
        float nota;


        nota = Float.parseFloat(JOptionPane.showInputDialog(null, "Digite su nota"));

        if ((nota >= 0) && (nota <= 2)){
            JOptionPane.showMessageDialog(null, "Su nota es insuficiente: "+nota);
        }
        else if ((nota >= 3) && (nota <= 4)){
            JOptionPane.showMessageDialog(null, "Su nota es suficiente: "+nota);
        }
        else if ((nota >= 5) && (nota <= 6)){
            JOptionPane.showMessageDialog(null, "Su nota es buena: "+nota);
        }
        else if ((nota >= 7) && (nota <= 8)){
            JOptionPane.showMessageDialog(null, "Su nota es notable: "+nota);
        }
        else {
            JOptionPane.showMessageDialog(null, "Su nota es sobresaliente");
        }
    }
}
